import requests
import json
import urllib.request

print('\n          Hello. This program will help you download photos from instagram ')


def loadInstagramPage(target_id, max_id=None):
    dl_url = 'https://www.instagram.com/' + target_id + '/?__a=1'
    if max_id:
        dl_url = dl_url + '&max_id=' + max_id
    data = json.load(urllib.request.urlopen(dl_url))
    return data

def loadInstagramPages(target_id, max_id=None, min_likes=5, page_count_limit=15):
    data = {}
    nodes = []
    for i in range(page_count_limit):
        print(i)
        max_id = data.get('user', {}).get('media', {}).get('page_info', {}).get('end_cursor', None)
        data = loadInstagramPage(target_id, max_id)
    
        for node in data['user']['media']['nodes']:
            if node['likes']['count'] < min_likes:
                continue
            nodes.append(node)

        if not data['user']['media']['page_info']['has_next_page']:
            break
    return nodes

def printUserinfo(target_id):
    dl_url = 'https://www.instagram.com/' + target_id + '/?__a=1'
    data = json.load(urllib.request.urlopen(dl_url))
    
    print('Profile photo  >>>',(data['user']['profile_pic_url_hd']))
    print('User Name,  >>>', (data['user']['username']))
    print('Full Name  >>>',(data['user']['full_name']))
    print('User ID  >>>',(data['user']['id']))
    print('Followed by  >>>',(data['user']['followed_by']['count']))
    print('Follows  >>>',(data['user']['follows']['count']))
    print('Publications   >>>',(data['user']['media']['count']))
    if data['user']['biography'] != None:
        print('User Biography  >>>',(data['user']['biography']))
    if data['user']['connected_fb_page'] != None:
        print(' FaceBoock Page  >>>',(data['user']['connected_fb_page']))
    actionProgram_menu(target_id)

def printPhotoUrl(target_id):
    min_likes = int(input('Enter min likes   '))
    page_count_limit = int(input('Enter page limit  '))
    
    res = loadInstagramPages(target_id, None, min_likes, page_count_limit)   

    for node in res:
        print(node['display_src'])
        print('Likes   ', (node['likes']['count']))
    actionProgram_menu(target_id)

def savePhoto(target_id):
    min_likes = int(input('Enter min likes   '))
    page_count_limit = int(input('Enter page limit  '))
    save_dir = input ('Enter a directory to save the photo   ')

    res = loadInstagramPages(target_id, None, min_likes, page_count_limit)   

    for node in res:
        loadPhoto_name = str(node['likes']['count']) + '_' + str(node['id']) + '_' + (target_id) + '_' + '.jpg'
        sv_dir_name = save_dir + loadPhoto_name
        photo_url = node['display_src']
        urllib.request.urlretrieve(photo_url, sv_dir_name)
    finish_massege = 'Photos were downloaded to your computer in   ' + save_dir
    print(finish_massege)
    actionProgram_menu(target_id)

def exit_program():
    print('Instagram saver is finished')


def actionProgram_menu(target_id):
    print('\n\n\n Select an action \n\n    Enter 1 for show user info \n    Enter 2 for show photo url \n    Enter 3 for save photp \n    Enter 0 for Exit\n')
    act_menu = input('Select an action  ')
    if act_menu == '1':
        printUserinfo(target_id)
    elif act_menu == '2':
         printPhotoUrl(target_id)
    elif act_menu == '3':
        savePhoto(target_id)
    elif act_menu == '0':
        exit_program()
    else:
        actionProgram_menu(target_id)




target_id = input('Enter target user id   ')
actionProgram_menu(target_id)
